\begin{thebibliography}{10}

\bibitem{Ariga:2019ufm}
Akitaka Ariga et~al.
\newblock {FASER: ForwArd Search ExpeRiment at the LHC}.
\newblock 1 2019.

\bibitem{Abreu:2019yak}
Henso Abreu et~al.
\newblock {Detecting and Studying High-Energy Collider Neutrinos with FASER at
  the LHC}.
\newblock {\em Eur. Phys. J. C}, 80(1):61, 2020.

\bibitem{Baltay:1988au}
C.~Baltay et~al.
\newblock {$\nu_\mu - \nu_e$ Universality in Charged Current Neutrino
  Interactions}.
\newblock {\em Phys. Rev. D}, 41:2653, 1990.

\bibitem{Aartsen:2013bka}
M.~G. Aartsen et~al.
\newblock {First observation of PeV-energy neutrinos with IceCube}.
\newblock {\em Phys. Rev. Lett.}, 111:021103, 2013.

\bibitem{Kodama:2007aa}
K.~Kodama et~al.
\newblock {Final tau-neutrino results from the DONuT experiment}.
\newblock {\em Phys. Rev. D}, 78:052002, 2008.

\bibitem{Agafonova:2018dfy}
N.~Agafonova et~al.
\newblock {Latest results of the OPERA experiment on nu-tau appearance in the
  CNGS neutrino beam}.
\newblock {\em SciPost Phys. Proc.}, 1:028, 2019.

\bibitem{Li:2017dbe}
Z.~Li et~al.
\newblock {Measurement of the tau neutrino cross section in atmospheric
  neutrino oscillations with Super-Kamiokande}.
\newblock {\em Phys. Rev. D}, 98(5):052006, 2018.

\bibitem{Novaes:1999yn}
S.~F. Novaes.
\newblock {Standard model: An Introduction}.
\newblock In {\em {10th Jorge Andre Swieca Summer School: Particle and
  Fields}}, pages 5--102, 1 1999.

\bibitem{PhysRevD.98.030001}
M.~Tanabashi et~al.
\newblock Review of particle physics.
\newblock {\em Phys. Rev. D}, 98:030001, Aug 2018.

\bibitem{Rajasekaran:2016ujt}
G~Rajasekaran.
\newblock {The Story of the Neutrino}.
\newblock 6 2016.

\bibitem{Reines:1960pr}
F.~Reines, C.~L. Cowan, F.~B. Harrison, A.~D. McGuire, and H.~W. Kruse.
\newblock {Detection of the free anti-neutrino}.
\newblock {\em Phys. Rev.}, 117:159--173, 1960.

\bibitem{Ismail:2020yqc}
Ahmed Ismail, Roshan Mammen~Abraham, and Felix Kling.
\newblock {Neutral Current Neutrino Interactions at FASER$\nu$}.
\newblock 12 2020.

\bibitem{Danev_2016}
Petar Danev, Andrzej Adamczak, Dimitar Bakalov, Emiliano Mocchiutti, Mihail
  Stoilov, and Andrea Vacchi.
\newblock Low-energy negative muon interaction with matter.
\newblock {\em Journal of Instrumentation}, 11(03):P03019–P03019, Mar 2016.

\bibitem{Chatterjee:1976kd}
L.~Chatterjee and B.~K. Bandyopadhyay.
\newblock {Interaction Characteristics of Muons with Matter for the Energy
  Range 10**9-MeV \ensuremath{>}= e \ensuremath{>}= 10**7-MeV}.
\newblock {\em Acta Phys. Austriaca}, 45:345--357, 1976.

\bibitem{2013IJMPA..2830035M}
Stephen {Myers}.
\newblock {The Large Hadron Collider 2008-2013}.
\newblock {\em International Journal of Modern Physics A}, 28(25):1330035--80,
  October 2013.

\bibitem{Adam:2002asd}
J.D. Adam, Thierry Boutboul, G.~Cavallari, Zinour Charifoulline, C.-H Denarie,
  Sandrine Le~Naour, Df~Leroy, L.R. Oberli, D.~Richter, A.P. Verweij, and
  R.~Wolf.
\newblock Status of the lhc superconducting cable mass production.
\newblock {\em Applied Superconductivity, IEEE Transactions on}, 12:1056 --
  1062, 04 2002.

\bibitem{Nielsen:2011kc}
Jason Nielsen.
\newblock {Fundamentals of LHC Experiments}.
\newblock In {\em {Theoretical Advanced Study Institute in Elementary Particle
  Physics}: {String theory and its Applications: From meV to the Planck
  Scale}}, pages 127--152, 6 2011.

\bibitem{LopezSola:2019xjb}
E.~Lopez~Sola et~al.
\newblock {Beam impact tests of a prototype target for the Beam Dump Facility
  at CERN: experimental setup and preliminary analysis of the online results}.
\newblock {\em Phys. Rev. Accel. Beams}, 22(12):123001, 2019.

\bibitem{Cristinziani:2020wvy}
Markus Cristinziani.
\newblock {The SHiP experiment at CERN}.
\newblock In {\em {3rd World Summit on Exploring the Dark Side of the
  Universe}}, 9 2020.

\bibitem{Fabbrichesi:2020wbt}
Marco Fabbrichesi, Emidio Gabrielli, and Gaia Lanfranchi.
\newblock {The Dark Photon}.
\newblock 5 2020.

\bibitem{FASER:2021cpr}
Henso Abreu et~al.
\newblock {The trigger and data acquisition system of the FASER experiment}.
\newblock 10 2021.

\bibitem{Ariga:2018pin}
Akitaka Ariga et~al.
\newblock {Technical Proposal for FASER: ForwArd Search ExpeRiment at the LHC}.
\newblock 12 2018.

\bibitem{RAL}
Jamie Boyd.
\newblock Forward search experiment at the lhc.
\newblock RAL Seminar, 2020.

\bibitem{FASER:2021ljd}
Henso Abreu et~al.
\newblock {The tracking detector of the FASER experiment}.
\newblock 12 2021.

\bibitem{Jackson:2005ec}
John~Neil Jackson.
\newblock {The ATLAS semiconductor tracker (SCT)}.
\newblock {\em Nucl. Instrum. Meth. A}, 541:89--95, 2005.

\bibitem{Abreu:2020ddv}
Henso Abreu et~al.
\newblock {Technical Proposal: FASERnu}.
\newblock 1 2020.

\bibitem{ICHEP:2020}
Akitaka Ariga.
\newblock Detecting and studying high-energy neutrinos with faser$\nu$ at the
  lhc.
\newblock ICHEP 2020, 2020.

\bibitem{Roesler:2000he}
Stefan Roesler, Ralph Engel, and Johannes Ranft.
\newblock {The Monte Carlo event generator DPMJET-III}.
\newblock In {\em {International Conference on Advanced Monte Carlo for
  Radiation Physics, Particle Transport Simulation and Applications (MC
  2000)}}, pages 1033--1038, 12 2000.

\bibitem{Pierog:2013ria}
T.~Pierog, Iu. Karpenko, J.~M. Katzy, E.~Yatsenko, and K.~Werner.
\newblock {EPOS LHC: Test of collective hadronization with data measured at the
  CERN Large Hadron Collider}.
\newblock {\em Phys. Rev. C}, 92(3):034906, 2015.

\bibitem{Ostapchenko:2004ss}
S.~Ostapchenko.
\newblock {QGSJET-II: Towards reliable description of very high energy hadronic
  interactions}.
\newblock {\em Nucl. Phys. B Proc. Suppl.}, 151:143--146, 2006.

\bibitem{Engel:2019dsg}
Felix Riehn, Ralph Engel, Anatoli Fedynitch, Thomas~K. Gaisser, and Todor
  Stanev.
\newblock {Hadronic interaction model Sibyll 2.3d and extensive air showers}.
\newblock {\em Phys. Rev. D}, 102(6):063002, 2020.

\bibitem{Sjostrand:2007gs}
Torbjorn Sjostrand, Stephen Mrenna, and Peter~Z. Skands.
\newblock {A Brief Introduction to PYTHIA 8.1}.
\newblock {\em Comput. Phys. Commun.}, 178:852--867, 2008.

\bibitem{FASER:2021mtu}
Henso Abreu et~al.
\newblock {First neutrino interaction candidates at the LHC}.
\newblock 5 2021.

\bibitem{Dabrowski:2003sk}
W.~Dabrowski.
\newblock {Readout of silicon strip detectors}.
\newblock {\em Nucl. Instrum. Meth. A}, 501:167--174, 2003.

\bibitem{Kobayashi_2012}
T.~Kobayashi, Y.~Komori, K.~Yoshida, K.~Yanagisawa, J.~Nishimura, T.~Yamagami,
  Y.~Saito, N.~Tateyama, T.~Yuda, and R.~J. Wilkes.
\newblock {OBSERVATIONS} {OF} {HIGH}-{ENERGY} {COSMIC}-{RAY} {ELECTRONS} {FROM}
  30 {GeV} {TO} 3 {TeV} {WITH} {EMULSION} {CHAMBERS}.
\newblock {\em The Astrophysical Journal}, 760(2):146, nov 2012.

\bibitem{jsaps}
John Spencer.
\newblock Studying neutrino cross-sections with faser and fasernu.
\newblock APS April Meeting, 2021.

\bibitem{jsdpf}
John Spencer.
\newblock Measuring neutrino-nucleon cross-sections with fasernu.
\newblock APS Division of Particles and Fields, 2021.

\bibitem{Ariga:2018zuc}
Akitaka Ariga et~al.
\newblock {Letter of Intent for FASER: ForwArd Search ExpeRiment at the LHC}.
\newblock 11 2018.

\bibitem{Aad:2012tfa}
Georges Aad et~al.
\newblock {Observation of a new particle in the search for the Standard Model
  Higgs boson with the ATLAS detector at the LHC}.
\newblock {\em Phys. Lett. B}, 716:1--29, 2012.

\bibitem{Freese:2017idy}
Katherine Freese.
\newblock {Status of Dark Matter in the Universe}.
\newblock {\em Int. J. Mod. Phys.}, 1(06):325--355, 2017.

\bibitem{Zwicky:1933gu}
F.~Zwicky.
\newblock {Die Rotverschiebung von extragalaktischen Nebeln}.
\newblock {\em Helv. Phys. Acta}, 6:110--127, 1933.

\bibitem{Eidemuller:1999mx}
M.~Eidemuller, Hans~Gunter Dosch, and M.~Jamin.
\newblock {The Field strength correlator from QCD sum rules}.
\newblock {\em Nucl. Phys. B Proc. Suppl.}, 86:421--425, 2000.

\bibitem{Guzey:2005vz}
V.~Guzey and M.~V. Polyakov.
\newblock {SU(3) systematization of baryons}.
\newblock 12 2005.

\bibitem{Greiner:1993qp}
W.~Greiner and Berndt Muller.
\newblock {\em {Gauge theory of weak interactions}}.
\newblock 1993.

\bibitem{Pich:2005mk}
A.~Pich.
\newblock {The Standard model of electroweak interactions}.
\newblock In {\em {2004 European School of High-Energy Physics}}, pages 1--48,
  2 2005.

\bibitem{Bednyakov:2007pz}
V.~A. Bednyakov, N.~D. Giokaris, and A.~V. Bednyakov.
\newblock {On Higgs mass generation mechanism in the Standard Model}.
\newblock {\em Phys. Part. Nucl.}, 39:13--36, 2008.

\bibitem{Banados:2016zim}
M\'aximo Ba\~nados and Ignacio~A. Reyes.
\newblock {A short review on Noether\textquoteright{}s theorems, gauge
  symmetries and boundary terms}.
\newblock {\em Int. J. Mod. Phys. D}, 25(10):1630021, 2016.

\bibitem{Laudisio:2017}
Fulvio Laudisio.
\newblock High precision reconstruction of electromagnetic showers in the
  nuclear emulsions of the opera experiment.
\newblock page 640, 11 2017.

\bibitem{Juget:2009}
Fr{\'{e}}d{\'{e}}ric Juget.
\newblock Electromagnetic shower reconstruction with emulsion films in the
  {OPERA} experiment.
\newblock {\em Journal of Physics: Conference Series}, 160:012033, apr 2009.

\bibitem{ai2019acts}
Xiaocong Ai.
\newblock Acts: A common tracking software, 2019.

\bibitem{Adams:2016ekx}
Corey~James Adams.
\newblock {\em {First Detection of Low Energy Electron Neutrinos in Liquid
  Argon Time Projection Chambers}}.
\newblock PhD thesis, Yale U., 2016.

\bibitem{Spano:2013}
Francesco Spanò.
\newblock Unfolding in particle physics: A window on solving inverse problems.
\newblock {\em EPJ Web of Conferences}, 55:03002--, 07 2013.

\end{thebibliography}
